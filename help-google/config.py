import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'eyaMei6Que7ohrur9Neiphaxutiegi'
    RECAPTCHA_ENABLED = True
    RECAPTCHA_SITE_KEY = os.environ.get('RECAPTCHA_PUBLIC_KEY') 
    RECAPTCHA_SECRET_KEY = os.environ.get('RECAPTCHA_SECRET_KEY') 
    RECAPTCHA_THEME = "dark"
    RECAPTCHA_TYPE = "image"
    RECAPTCHA_RTABINDEX = 10
    FLASK_DEBUG = 1

