FROM python:2-alpine
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
RUN pip install gunicorn
RUN addgroup -S mctf && adduser -S -u 1001 -G mctf mctf
USER mctf
WORKDIR /app/help-google
EXPOSE 5000
CMD ["gunicorn", "wsgi:app", "--bind", "0.0.0.0:5000"]
