from flask import redirect
from app import app


@app.errorhandler(404)
def not_found_error(error):
    return redirect("/", code=302)

@app.errorhandler(500)
def internal_error(error):
    return redirect("/", code=302)
