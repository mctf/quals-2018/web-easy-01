from flask import Flask
from config import Config
from flask_recaptcha import ReCaptcha

app = Flask(__name__)
app.config.from_object(Config)

recaptcha = ReCaptcha(app=app)

from app import routes, errors
