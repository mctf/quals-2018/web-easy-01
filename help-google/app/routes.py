from flask import render_template, request, session, send_from_directory
from app import app, recaptcha
import os
import datetime

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    if request.method == 'GET' and not session.has_key('counter'):
            return render_template('index.html', title='Let\'s help Google', prolog=True)
    if request.method == 'POST':
        if recaptcha.verify():
            # SUCCESS
            if not session.has_key('counter'):
                session['counter'] = 1
                session['m'] = datetime.datetime.now().timetuple()[4] % 3
                session['s'] = datetime.datetime.now().timetuple()[5]

            elif datetime.datetime.now().timetuple()[4] % 3 < session['m'] or \
                datetime.datetime.now().timetuple()[5] < session['s']:
                session.pop('counter', None)
                return render_template('index.html', title='Let\'s help Google',
                                       status='You ran out of time. Try faster!')

            session['counter'] += 1
            if session['counter'] >= 60:
                return render_template('index.html', title='Let\'s help Google',
                                       status='That\'s your flag: ' + str(os.environ['FLAG']))

            return render_template('index.html', title='Let\'s help Google',
                                   status='You solve it! Your count: ' + str(session['counter']))
        else:
            # FAILED
            session.pop('counter', None)
            return render_template('index.html', title='Let\'s help Google', status='It seems that you are robot (-_-)')
    else:
        return render_template('index.html', title='Let\'s help Google', status="Let's solve Captcha!")

@app.route('/.hg/<path:filename>')
def custom_static(filename):
    return send_from_directory(os.path.join(app.root_path,'.hg'), filename)